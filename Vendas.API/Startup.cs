using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Vendas.API.Data;
using Vendas.API.Interfaces.Repository;
using Vendas.API.Models;
using Vendas.API.Util;
using Vendas.API.Repository;

namespace Vendas.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IRepository, Repository.Repository>()
                    .AddDbContext<DataContext>(context => context.UseInMemoryDatabase("MyDatabase"));
            
            
            services.AddControllers()
                .AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            services.AddSwaggerGen(options => 
            {
                options.SwaggerDoc("VendasAPI", new Microsoft.OpenApi.Models.OpenApiInfo()
                {
                    Title = "Vendas API",
                    Version = "1.0"
                });
            });   
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                /*var context = app.ApplicationServices.GetService<DataContext>();
                AdicionarDadosTeste(context);*/
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseSwagger()
            .UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/VendasAPI/swagger.json", "vendasapi");
                options.RoutePrefix = "";
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            var serviceScopeFactory = app.ApplicationServices.GetService<IServiceScopeFactory>();
            using (var scope = serviceScopeFactory.CreateScope())
            {
                
                AdicionarDadosTeste(scope.ServiceProvider.GetService<DataContext>());
               
            }
        }
        private static void AdicionarDadosTeste(DataContext context)
        {
            context.Vendedores.AddRange(new List<Vendedor>(){
                    new Vendedor(1, "12345678900", "Vendedor 1", "vendedor1@loja.com", "telefoneVendedor1"),
                    new Vendedor(2, "12345678901", "Vendedor 2", "vendedor2@loja.com", "telefoneVendedor2"),
                    new Vendedor(3, "12345678902", "Vendedor 3", "vendedor3@loja.com", "telefoneVendedor3"),
                    new Vendedor(4, "12345678903", "Vendedor 4", "vendedor4@loja.com", "telefoneVendedor4")});
            
            context.Itens.AddRange(
                new List<Item>(){
                new Item(1, "Item 1"),
                new Item(2, "Item 2"),
                new Item(3, "Item 3"),
                new Item(4, "Item 4")});

           context.Vendas.AddRange(
                new List<Venda>(){
                new Venda(1, 1, DateTime.Now, Enums.EnumStatus.Aguardando_pagamento.ToString().Replace('_',' ')),
                new Venda(2, 1, DateTime.Now, Enums.EnumStatus.Aguardando_pagamento.ToString().Replace('_',' ')),
                new Venda(3, 3, DateTime.Now, Enums.EnumStatus.Aguardando_pagamento.ToString().Replace('_',' ')),
                new Venda(4, 3, DateTime.Now, Enums.EnumStatus.Aguardando_pagamento.ToString().Replace('_',' '))});

            context.VendasItens.AddRange(
                new List<VendaItens>(){
                new VendaItens(1,1),
                new VendaItens(1, 2),
                new VendaItens(1, 4),
                new VendaItens(2, 2),
                new VendaItens(3, 3),
                new VendaItens(4, 1),
                new VendaItens(4, 4)});

            context.SaveChanges();
        }
    }
}
