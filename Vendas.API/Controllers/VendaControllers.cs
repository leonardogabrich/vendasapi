using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Vendas.API.Models;
using System;
using System.Linq;
using Vendas.API.Data;
using Vendas.API.Interfaces.Repository;
using Vendas.API.Util;

namespace Vendas.API.Controllers
{
    [ApiController]
    [Route("api/venda")]
    public class VendaControllers : Controller
    {
        private readonly DataContext _context;
        public readonly IRepository _Repo;
        public VendaControllers(DataContext context, IRepository repo)
        {
            _Repo = repo;
            _context = context;
        }
        public List<Venda> Vendas = new List<Venda>();

        [HttpGet("ObtemTodasVendas")]
        public IActionResult Get()
        {
            return Ok(_Repo.ObtemTodasAsVendas(true));
        }
        [HttpGet("BuscarVenda/{id}")]
        public IActionResult Get(int Id)
        {
            var venda = _Repo.ObtemVendaPorId(Id);
            if (venda == null)
                return BadRequest($"Nenhuma venda com id '{Id}' encontrada");

            return Ok(venda);
        }

        [HttpPost("CriaVenda")]
        public IActionResult Post(Venda venda)
        {
            try
            {    
                if (venda == null)
                    return BadRequest($"Objeto inválido para Venda");
                
                _Repo.Add(venda);

                if(_Repo.SaveChanges())
                    return Ok($"Nova venda incluída - Venda {venda.Id}") ;

                return BadRequest("Erro ao salvar venda");
            }
            catch(Exception e)
            {
                return BadRequest($"Erro ao incluir venda: {e.Message}");          
            }
        }
        
        [HttpPut("AtualizaStatusVenda")]
        public IActionResult Put(Venda venda )
        {
            try
            {    
                if (venda == null)
                    return BadRequest($"Objeto inválido para Venda");

                if(!Enum.IsDefined(typeof(Enums.EnumStatus), venda.Status.Replace(" ","_")))
                     return BadRequest($"Status inválido");

                if(_Repo.AtualizaVenda(venda.Id,(Enums.EnumStatus) Enum.Parse(typeof(Enums.EnumStatus), venda.Status.Replace(" ","_"), true) ))
                    if(_Repo.SaveChanges())
                        return Ok($"Nova venda incluída - Venda {venda.Id}") ;

                return BadRequest("Erro ao salvar venda");
            }
            catch(VendaCheckException e)
            {
                return BadRequest($"Erro ao incluir venda: {e.Message}");          
            }
            catch(Exception e)
            {
                return BadRequest($"Erro grave ao incluir venda: {e.Message}");          
            }
        }
    }
}