using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Vendas.API.Models;
using Vendas.API.Util;

namespace Vendas.API.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options){}

        public DbSet<Item> Itens { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<VendaItens> VendasItens { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<VendaItens>().HasKey(i => new{i.ItemId, i.VendaId});

            builder.Entity<Vendedor>().HasData(
                new List<Vendedor>(){
                    new Vendedor(1, "12345678900", "Vendedor 1", "vendedor1@loja.com", "telefoneVendedor1"),
                    new Vendedor(2, "12345678901", "Vendedor 2", "vendedor2@loja.com", "telefoneVendedor2"),
                    new Vendedor(3, "12345678902", "Vendedor 3", "vendedor3@loja.com", "telefoneVendedor3"),
                    new Vendedor(4, "12345678903", "Vendedor 4", "vendedor4@loja.com", "telefoneVendedor4")});
            
            builder.Entity<Item>().HasData(
                new List<Item>(){
                new Item(1, "Item 1"),
                new Item(2, "Item 2"),
                new Item(3, "Item 3"),
                new Item(4, "Item 4")});

            builder.Entity<Venda>().HasData(
                new List<Venda>(){
                new Venda(1, 1, DateTime.Now, Enums.EnumStatus.Aguardando_pagamento.ToString()),
                new Venda(2, 1, DateTime.Now, Enums.EnumStatus.Aguardando_pagamento.ToString()),
                new Venda(3, 3, DateTime.Now, Enums.EnumStatus.Aguardando_pagamento.ToString()),
                new Venda(4, 3, DateTime.Now, Enums.EnumStatus.Aguardando_pagamento.ToString())});

            builder.Entity<VendaItens>().HasData(
                new List<VendaItens>(){
                new VendaItens(1,1),
                new VendaItens(1, 2),
                new VendaItens(1, 4),
                new VendaItens(2, 2),
                new VendaItens(3, 3),
                new VendaItens(4, 1),
                new VendaItens(4, 4)});

        }
    }
}
