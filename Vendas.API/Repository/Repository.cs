using Vendas.API.Data;
using Vendas.API.Models;
using Vendas.API.Util;
using Vendas.API.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Vendas.API.Repository
{
    public class Repository : IRepository
    {
        public DataContext _context { get; }

        public Repository(DataContext context)
        {
            _context = context;

        }
        public bool AtualizaVenda(int id, Enums.EnumStatus novoStatus)
        {
            var venda = _context.Vendas.Where(a => a.Id == id).FirstOrDefault();

            if(venda == null)
                throw new VendaCheckException($"Venda {id} não encontrada");

            Enums.EnumStatus statusAtual = 
                (Enums.EnumStatus) Enum.Parse(typeof(Enums.EnumStatus), venda.Status.Replace(" ","_"), true);

            if((statusAtual == Enums.EnumStatus.Aguardando_pagamento && (novoStatus != Enums.EnumStatus.Pagamento_aprovado 
                    && novoStatus != Enums.EnumStatus.Cancelado)) || 
                (statusAtual == Enums.EnumStatus.Pagamento_aprovado && (novoStatus != Enums.EnumStatus.Enviado_para_transportadora 
                    && novoStatus != Enums.EnumStatus.Cancelado)) ||
                (statusAtual == Enums.EnumStatus.Enviado_para_transportadora && (novoStatus != Enums.EnumStatus.Entregue)))
            {
                throw new VendaCheckException("Novo status inválido!");
            }
            venda.Status = novoStatus.ToString().Replace("_", " ");
            return true;
        }

        public void Add<T>(T entity) where T : class
        {
            if(entity is Venda)
                AddVenda(entity as Venda);    
            else
                _context.Add(entity);
        }
        private void AddVenda(Venda entity)
        {

            
            if(entity.VendaItens.Any(itens => !_context.Itens.Select(i => i.Id).Contains(itens.ItemId)))
                throw new VendaCheckException("Item não existente.");  

            if(_context.Vendas.Any(i => i.Id == entity.Id))
                throw new VendaCheckException("Venda já existente.");  
            
            if(_context.Vendedores.Any(i => i.Id == entity.VendedorId))
                entity.Vendedor = null;  
            else if(string.IsNullOrEmpty(entity.Vendedor.Cpf) || 
                string.IsNullOrEmpty(entity.Vendedor.Email) ||
                string.IsNullOrEmpty(entity.Vendedor.Nome) ||
                string.IsNullOrEmpty(entity.Vendedor.Telefone))
                throw new VendaCheckException("Vendedor precisa de ter todos os campos preenchidos");
            // Limpa dados do vendedor para não atualizar.
  
            
            if(entity.VendaItens == null || !entity.VendaItens.Any())
                throw new VendaCheckException("Venda sem nenhum item informado.");
                     
            _context.Add(entity);
        }

        public void Update<T>(T entity) where T : class
        {
             _context.Update(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() > 0;;
        }

        public List<Venda> ObtemTodasAsVendas(bool incluirItens)
        {
            IQueryable<Venda> query = _context.Vendas;

            if(incluirItens)
                query = query
                .Include(a => a.Vendedor)
                .Include(a => a.VendaItens)
                .ThenInclude(a => a.Item);

            query = query.AsNoTracking();
            
            return query.OrderByDescending(a => a.Id).ToList();
        }
        public Venda ObtemVendaPorId(int id)
        {
            IQueryable<Venda> query = _context.Vendas;

            query = query
            .Include(a => a.Vendedor)
            .Include(a => a.VendaItens)
            .ThenInclude(a => a.Item);

            query = query.AsNoTracking().Where(a => a.Id == id);
            
            return query.FirstOrDefault();
        }
    }

}