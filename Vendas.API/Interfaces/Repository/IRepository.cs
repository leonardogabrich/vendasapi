using System.Collections.Generic;
using Vendas.API.Models;
using Vendas.API.Util;

namespace Vendas.API.Interfaces.Repository
{
    public interface IRepository 
    {
        void Add<T> (T entity) where T : class;
        void Update<T> (T entity) where T : class;
        void Delete<T> (T entity) where T : class;
        bool SaveChanges();
        public bool AtualizaVenda(int id, Enums.EnumStatus novoStatus);
        public List<Venda> ObtemTodasAsVendas(bool incluirItens);
        public Venda ObtemVendaPorId(int id);

    }
}