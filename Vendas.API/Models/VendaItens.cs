namespace Vendas.API.Models
{
    public class VendaItens
    {
        public VendaItens() { }
        public VendaItens(int vendaId, int itemId)
        {
            this.VendaId = vendaId;
            this.ItemId = itemId;
        }
        public int VendaId { get; set; }
        public Venda Venda { get; set; }
        public int ItemId { get; set; }
        public Item Item { get; set; }
    }
}