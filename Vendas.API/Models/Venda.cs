using System;
using System.Collections.Generic;

namespace Vendas.API.Models
{
    public class Venda
    {
        public Venda() { }

        public Venda(int id, int vendedorId, DateTime data, string status)
        {
            this.Id = id;
            this.VendedorId = vendedorId;
            this.Data = data;
            this.Status = status;

        }
        public int Id { get; set; }
        public int VendedorId { get; set; }
        public Vendedor Vendedor { get; set; }
        public IEnumerable<VendaItens> VendaItens { get; set; }
        public DateTime Data { get; set; }
        public string Status { get; set; }
    }
}