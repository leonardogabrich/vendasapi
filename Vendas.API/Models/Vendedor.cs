namespace Vendas.API.Models
{
    public class Vendedor
    {
        public Vendedor(){}

        public Vendedor(int id, string cpf, string nome, string email, string telefone) 
        {
            this.Id = id;
                this.Cpf = cpf;
                this.Nome = nome;
                this.Email = email;
                this.Telefone = telefone;
               
        }
        public int Id { get; set; }
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
    }
}