using System;

namespace Vendas.API.Util
{
    public class Enums
    {
        public enum EnumStatus
        {
            Aguardando_pagamento,
            Pagamento_aprovado,
            Enviado_para_transportadora,
            Entregue,
            Cancelado
        }
    }

}