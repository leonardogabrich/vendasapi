namespace Vendas.API.Util
{
    public class VendaCheckException : System.Exception
    {
        public VendaCheckException() { }
        public VendaCheckException(string message) : base(message) { }
        public VendaCheckException(string message, System.Exception inner) : base(message, inner) { }
        protected VendaCheckException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}